const config = require('./../knexfile.js')[process.env.NODE_ENV];
const db = require('knex')(config);

module.exports = db;
