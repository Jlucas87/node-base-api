const express = require('express');

const router = express.Router();

router.use('/api', require('./user'));
router.use('/health', require('./health'));
// Add additional api route methods here

module.exports = router;
