# Node Base API

## Instructions for running the project

You must have NodeJS (https://nodejs.org/en/download/) installed on your machine in order to run this project locally.
In the top level directory of the project folder, run the command
```sh
npm install
```

You may then run any of the following Available Scripts listed below.

## Available Scripts

In the project directory, you can run:

### `npm run start`

Runs the app in development mode.<br>
The API will be accessible at [http://localhost:3001](http://localhost:3001)

### `npm run make-migration <<migration name>>`

This command allows you to create a new migration for the database.

### `npm run migrate`

Runs all of the available/latest migrations ensuring your database is up to date.
