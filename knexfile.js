require('dotenv').config();
const dbConfiguration = require('./config/database.js');

module.exports = {
  development: {
    client: 'mysql',
    connection: dbConfiguration.development,
    searchPath: 'knex, public',
    seeds: {
      directory: './seeds/dev'
    },
    migrations: {
      tableName: 'knex_migrations'
    },
  },
};
