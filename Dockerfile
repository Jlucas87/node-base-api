# Pull in the latest image for node
FROM node:latest

# Define the working directory
WORKDIR /usr/app

# Put all files in the current directory into the workdir of the image
COPY . .

# Install node dependencies
RUN npm install

# The command the container will run
CMD npm run start
