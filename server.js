require('dotenv').config();
const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
const bodyParser = require('body-parser');
const helmet = require('helmet')
const db = require('./config/db');
const cors = require('cors');

app.use(cors());

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json({ limit: '10mb' }));

app.use(helmet())

app.use(require('./controllers/'));

app.listen(port, () => {
  console.log('Node API started on: ' + port);
});
